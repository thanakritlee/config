# Run bfg
alias bfg='java -jar /usr/local/bfg-1.13.0.jar'
alias postman='~/Postman/Postman-1547630713039/Postman'
alias umlet='~/umlet-standalone-14.2/Umlet/umlet.sh'
alias show-disk-usage='du -hs ./* | sort -h'